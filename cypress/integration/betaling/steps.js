import { Given, When, Then, And } from "cypress-cucumber-preprocessor/steps";

// Vellykket kjøp
Given(/^at jeg har lagt inn varer i handlekurven$/, () => {
    cy.visit('http://localhost:8080');
    
    cy.get('#product').select('Smørbukk');
    cy.get('#quantity').clear().type('3');
    cy.get('#saveItem').click();

    cy.get('#product').select('Stratos');
    cy.get('#quantity').clear().type('2');
    cy.get('#saveItem').click();
});

And(/^trykket på Gå til betaling$/, () => {
    cy.get('#goToPayment').click();
});

When(/^jeg legger inn navn, adresse, postnummer, poststed og kortnummer$/, () => {
    cy.get('#fullName').clear().type('Ola Nordmann');
    cy.get('#address').clear().type('Veigaten 23');
    cy.get('#postCode').clear().type('1234');
    cy.get('#city').clear().type('Oslo');
    cy.get('#creditCardNo').clear().type('1111222233334444');
});

And(/^trykker på Fullfør kjøp$/, () => {
    cy.get('form').submit();
});

Then(/^skal jeg få beskjed om at kjøpet er registrert$/, () => {
    cy.get('.confirmation').should('exist');
});

// Feil betalingsinformasjon
When(/^jeg legger inn ugyldige verdier i feltene$/, () => {
    cy.get('#fullName').clear().blur();
    cy.get('#address').clear().blur();
    cy.get('#postCode').clear().blur();
    cy.get('#city').clear().blur();
    cy.get('#creditCardNo').clear().blur();
});

Then(/^skal jeg få feilmeldinger for disse$/, () => {
    cy.get('#fullName').parent().contains('Feltet må ha en verdi');
    cy.get('#address').parent().contains('Feltet må ha en verdi');
    cy.get('#postCode').parent().contains('Feltet må ha en verdi');
    cy.get('#city').parent().contains('Feltet må ha en verdi');
    cy.get('#creditCardNo').parent().contains('Kredittkortnummeret må bestå av 16 siffer');
});